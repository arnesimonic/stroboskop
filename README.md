# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://arnesimonic@bitbucket.org/arnesimonic/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/arnesimonic/stroboskop/commits/7b4bd171e273d3c27e4c844a5a0a91a6741449e4

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/arnesimonic/stroboskop/commits/6107c2b241618f85e708757e68b1b33ec23afac5

Naloga 6.3.2:
https://bitbucket.org/arnesimonic/stroboskop/commits/bbf5dc602fdb641e5259986aa952e5cc79094bbf

Naloga 6.3.3:
https://bitbucket.org/arnesimonic/stroboskop/commits/7c9cb26f31a045aa7c895452f934df4511d68c8f

Naloga 6.3.4:
https://bitbucket.org/arnesimonic/stroboskop/commits/ac0d2c11df2c07a7af7ac7a3e4a88445d20cded0

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/arnesimonic/stroboskop/commits/95e7e6c672d63e9118cb27c37d97abc8e1994b18

Naloga 6.4.2:
https://bitbucket.org/arnesimonic/stroboskop/commits/1b44c47bd523abebcca82d1e2a8f4120864873f7

Naloga 6.4.3:
https://bitbucket.org/arnesimonic/stroboskop/commits/58a1074a2f86d8d635adf8a13e69d307776634bc

Naloga 6.4.4:
https://bitbucket.org/arnesimonic/stroboskop/commits/cfd9fca6e1950e16d95acf390541873558508622